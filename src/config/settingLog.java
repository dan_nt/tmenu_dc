package config;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;

public class settingLog
{
  Writer output;
  Date date;
  SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd"); 
  
  public settingLog()
  {
    this.date = new Date();
    try
    {
      this.output = new BufferedWriter(new FileWriter("log/tmenu_dc_log." + this.sdf.format(this.date).toString(), true));
    }
    catch (IOException e)
    {
      System.out.println(e);
    }
  }
  
  public void writeLog(String text)
  {
    try
    {
      this.output.append(this.date.toString() + ": " + text + System.lineSeparator());
    }
    catch (IOException e)
    {
      System.out.println(e);
    }
  }
  
  public void closeLog()
  {
    try
    {
      this.output.close();
    }
    catch (IOException e)
    {
      System.out.println(e);
    }
  }
}
