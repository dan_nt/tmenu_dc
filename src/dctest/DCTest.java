/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templatdr
 * and open the template in the editor.
 */
package dctest;

/**
 *
 * @author ag111
 */
import config.settingDB;
import config.settingLog;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.connectlocal;

public class DCTest {

    /**
     * @param args the command line arguments
     */

    static connectlocal cnl = null;
    static ResultSet rs = null;
    static settingLog setlog;
    static settingDB setDB = new settingDB();
    
    
    static String msisdn = null;
    static String xminsec = null;
    static int minseconds = 0;
    static String fulltrxdate = null;
    static float avg_tps = 0;
    static int max_tps = 0;
    static String groupTps = "";
    static String groupname = "";
    static String transdate = null;
    static String dcode = null;
    static String hostname = null;
    static double count_data = 0;
    static double count_insert = 0;
    
    static HashMap<String, String> data = new HashMap<String, String>();
    static HashMap<String, String> dataTPS = new HashMap<String, String>();
    static HashMap<String, String> tpsGrouping = new HashMap<String, String>();
    static HashMap<String, String> arrlist = new HashMap<String, String>();
    
        
    static int hit = 0;
    
    
    
    private static final String FILENAME = setDB.getDirname();
    private static final String DIRCODENAME = setDB.getDcname();
//    private static final String FILENAME = "C:\\Users\\ag111\\Documents\\file server\\201810241500.TDR";
//    private static final String FILENAME = "C:\\Users\\ag111\\Documents\\file server\\sample.txt";
//    private static final String DIRCODENAME = "C:\\Users\\ag111\\Documents\\file server\\directcode.dat";

    public static void main(String[] args) throws SQLException, FileNotFoundException {
        
        
        setlog = new settingLog();
        setlog.writeLog("Start TMENU DC Application");
        BufferedReader br = null;
        FileReader fr = null;
        Calendar c = Calendar.getInstance();
        SimpleDateFormat format1 = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat format2 = new SimpleDateFormat("yyyyMMddHH");        
//        c.add(5, -5);
                
        try {
            fr = new FileReader(FILENAME + format2.format(c.getTime())+".log");
//            fr = new FileReader(FILENAME);
            br = new BufferedReader(fr);
            String sCurrentLine;
            br = new BufferedReader(new FileReader(FILENAME+format2.format(c.getTime())+".log"));
//            br = new BufferedReader(new FileReader(FILENAME));
            System.out.println("start time : "+c.getTime());
            System.out.println("Reading and processing file.."+FILENAME+format2.format(c.getTime())+".log");
            
            HashMap<String, String> dcodes = new HashMap<String, String>();
            dcodes = s_dcodes();
            while ((sCurrentLine = br.readLine()) != null) {
                                count_data++;

                                String[] value_split = sCurrentLine.split("\\|");
                                if ("".equals(value_split[14]) || "".equals(value_split[2]))
                                    continue;
                                else if(!"628".equals(value_split[14].substring(0,3)))
                                    continue;
                                if(value_split[0].contains("DATE"))
                                    continue;
                                if(value_split[2].contains("#"))
                                    continue;
                                getTPS(value_split);
                                get_unique_msisdn(value_split,dcodes);
            }
            insertTPS(dataTPS);
            insertUniqueMSISDN(data);
            Calendar d = Calendar.getInstance();
            System.out.println("end time : "+d.getTime());

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)
                    br.close();

                if (fr != null)
                    fr.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

    }
    
      
    public static HashMap<String, String> s_dcodes() {
        BufferedReader br = null;
        FileReader fr = null;
        String dcodes[] = null;
        try {
            fr = new FileReader(DIRCODENAME);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DCTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        br = new BufferedReader(fr);
        String sCurrentLine;
        try {
            br = new BufferedReader(new FileReader(DIRCODENAME));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DCTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        HashMap<String, String> list_dcode = new HashMap<String, String>();
        try {
            while ((sCurrentLine = br.readLine()) != null) {
                dcodes = sCurrentLine.split("\\=", -1);
                list_dcode.put(dcodes[0], "OK");
            }
            return list_dcode;
        } catch (IOException ex) {
            Logger.getLogger(DCTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static void get_unique_msisdn(String[] value_split, HashMap<String, String> dcodes){
        
//        if ("".equals(value_split[14])){
//            return;
//        }
//        else if(!"628".equals(value_split[14].substring(0,3))){
//            return;
//        }

        //System.out.println(value_split[0]);
        transdate = value_split[0].substring(0,8)+value_split[0].substring(9,11);
        
        //msisdn = sCurrentLine.substring(62,76);
        msisdn = value_split[14];
        msisdn = msisdn.replaceAll(" ", "");
        //System.out.println(msisdn);
        //dcode = sCurrentLine.substring(209, 240);
        dcode = value_split[7];
        dcode = dcode.replaceAll(" ", "");
        if(dcode.length()>=5){
            String groupname = msisdn+"_"+dcode;
            data.put(groupname + "_00",transdate);
            data.put(groupname + "_01",msisdn);
            
            if(!"OK".equals(dcodes.get(dcode)))
                dcode=value_split[2];

            data.put(groupname + "_02",dcode);

            //total transaction
            if(data.get(groupname+"_3") == null){
                data.put(groupname+"_3", "1");
            }else{
                hit = Integer.parseInt(data.get(groupname+"_3"))+1;
                data.put(groupname+"_3", String.valueOf(hit));
            }
        }
    }
    
    
    public static void getTPS(String[] value_split){
//        if(value_split[0].contains("DATE"))
//            return;
        dcode=value_split[2];
//        if(dcode.contains("#"))
//            return;

        hostname = value_split[12];
        transdate = value_split[0].substring(0,8)+value_split[0].substring(9,11);
        minseconds = Integer.parseInt(value_split[0].substring(12,14)+value_split[0].substring(15,17));

        if(minseconds>=0 & minseconds < 500)
            xminsec="00";
        else if(minseconds>=500 & minseconds < 1000)
            xminsec="05";
        else if(minseconds>=1000 & minseconds < 1500)
            xminsec="10";
        else if(minseconds>=1500 & minseconds < 2000)
            xminsec="15";
        else if(minseconds>=2000 & minseconds < 2500)
            xminsec="20";
        else if(minseconds>=2500 & minseconds < 3000)
            xminsec="25";
        else if(minseconds>=3000 & minseconds < 3500)
            xminsec="30";
        else if(minseconds>=3500 & minseconds < 4000)
            xminsec="35";
        else if(minseconds>=4000 & minseconds < 4500)
            xminsec="40";
        else if(minseconds>=4500 & minseconds < 5000)
            xminsec="45";
        else if(minseconds>=5000 & minseconds < 5500)
            xminsec="50";
        else if(minseconds>=5500 )
            xminsec="55";

        groupname = transdate+xminsec+"_"+dcode+"_"+hostname;
        dataTPS.put(groupname + "_00",transdate+xminsec);
        dataTPS.put(groupname + "_01",dcode);
        dataTPS.put(groupname + "_02",hostname);

        //average TPS
        if(dataTPS.get(groupname+"_03") == null){
            dataTPS.put(groupname+"_03", value_split[13]); //total transaction in five minutes
            avg_tps = (Float.parseFloat(value_split[13]))/300;
            if((int)avg_tps == 0 ) avg_tps = 1;
                dataTPS.put(groupname+"_04", String.valueOf((int)avg_tps)); //average TPS
        }else{
            hit = (Integer.parseInt(dataTPS.get(groupname+"_03"))+Integer.parseInt(value_split[13]));
            avg_tps = (Float.parseFloat(dataTPS.get(groupname+"_03"))+Float.parseFloat(value_split[13]))/300;
            if((int)avg_tps == 0 ) avg_tps = 1;
                dataTPS.put(groupname+"_03", String.valueOf(hit));
                dataTPS.put(groupname+"_04", String.valueOf((int)avg_tps));
        }
        //max TPS
        fulltrxdate = value_split[0].substring(0,8)+value_split[0].substring(9,11)+value_split[0].substring(12,14)+value_split[0].substring(15,17);
        groupTps = "TPS_"+fulltrxdate+"_"+dcode+"_"+hostname;
        tpsGrouping.put(groupTps+"_01", dcode);
        tpsGrouping.put(groupTps+"_02", hostname);

        //groupingTPS
        if(tpsGrouping.get(groupTps+"_00") == null)
            tpsGrouping.put(groupTps+"_00", value_split[13]);
        else {
            max_tps = (Integer.parseInt(tpsGrouping.get(groupTps+"_00"))+Integer.parseInt(value_split[13]));
            tpsGrouping.put(groupTps+"_00", String.valueOf(max_tps));
        }

        int curtime = Integer.parseInt(value_split[0].substring(9,11)+xminsec);
        int xtime = Integer.parseInt(groupTps.substring(12, 16));
        if(xtime >= curtime && xtime < curtime+5 && dataTPS.get(groupname+"_01") == tpsGrouping.get(groupTps+"_01") && dataTPS.get(groupname+"_02") == tpsGrouping.get(groupTps+"_02"))
        {
            if(dataTPS.get(groupname+"_05") == null)
                dataTPS.put(groupname+"_05", String.valueOf(max_tps)); //total transaction
        else
            if(Integer.parseInt(dataTPS.get(groupname+"_05")) < max_tps)
                dataTPS.put(groupname+"_05", String.valueOf(max_tps));
        }
//      if(value_split[2].contains("363") && groupTps.contains("201810311359")){
//          System.out.println(groupname+" : "+dataTPS.get(groupname+"_05"));
//          System.out.println("Max val: " + Collections.max(list,null)); 
//      }
    }
    
    public static void insertTPS(HashMap<String, String> dataDB){
        System.out.println("Inserting TPS data to db..");
        cnl = new connectlocal();
        cnl.connect();
        int i=1;
        Map<String, String> treeMap = new TreeMap<String, String>(dataDB);
        for (String str : treeMap.keySet()) {
            arrlist.put(String.valueOf(i), dataDB.get(str));
            if(i%6==0){
                cnl.insert_tps(arrlist.get("1"), arrlist.get("2"), arrlist.get("3"),arrlist.get("6"),arrlist.get("5"));
                count_insert++;
                i=0;
            }
            i++;
        }
        cnl.close();
        System.out.println("Count data TPS:"+count_data);
        System.out.println("Count insert TPS:"+count_insert);
    }
    
    public static void insertUniqueMSISDN(HashMap<String, String> dataMsisdn){
        System.out.println("Inserting Unique MSISDN data to db..");
        cnl = new connectlocal();
        cnl.connect();
        int i=1;
        Map<String, String> treeMap = new TreeMap<String, String>(dataMsisdn);
        for (String str : treeMap.keySet()) {
            arrlist.put(String.valueOf(i), dataMsisdn.get(str));
            if(i%4==0){
                cnl.insert_unique(arrlist.get("1"), arrlist.get("2"), arrlist.get("3"),arrlist.get("4"));
                count_insert++;
                i=0;
            }
            i++;
        }
        cnl.close();
        System.out.println("Count data MSISDN:"+count_data);
        System.out.println("Count insert MSISDN:"+count_insert);
    }
    
}
