package model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mulher
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import config.settingDB;
import config.settingLog;

public class connectlocal {
    Connection con = null;
    Statement st = null;
    ResultSet rs = null;
    

    
    settingDB setdb;
    settingLog setlog;
    //xxx;
    public void connect(){
        /*String url = "jdbc:mysql://localhost:3306/smp";
        String user = "root";
        String password = "root";
        try {
            
            con = DriverManager.getConnection(url, user, password);
            st = con.createStatement();
            con.setAutoCommit(false);
        } catch (SQLException ex) {
        }*/
        this.setdb = new settingDB();
        String url = this.setdb.getDbURL();
        String user = this.setdb.getUser();
        String password = this.setdb.getPass();
        this.setlog = new settingLog();
        try
        {
          this.con = DriverManager.getConnection(url, user, password);
          this.st = this.con.createStatement();
          this.con.setAutoCommit(false);
        }
        catch (SQLException e)
        {
          this.setlog.writeLog(e.toString());
          this.setlog.closeLog();
        }
    }
    
    public void close(){
        try {
                con.commit();
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex);
            }
    }
    
    public boolean insert_unique(String transdate, String msisdn,String servCode,String total){    
        try {
            
//            System.out.println("INSERT INTO `UNIQUE_MSISDN_DETAIL_10`"
//                    + "(`trans_date`,`msisdn`, `service_code`, `transaction`) "
//                    + "VALUES ('"+transdate+"','"+msisdn+"','"+servCode+"','"+total+"');");
            
            
            st.executeUpdate("INSERT INTO `UNIQUE_MSISDN_DETAIL_"+transdate.substring(4, 6)+"`"
                    + "(`trans_date`,`msisdn`, `service_code`, `transaction`) "
                    + "VALUES ('"+transdate+"','"+msisdn+"','"+servCode+"','"+total+"');");
            
            return true;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return false;
    }
    public boolean insert_tps(String transdate, String service_code, String hostname, String max_tps,String avg_tps){    
        try {
            
//            System.out.println("INSERT INTO `TMENU_TPS_`"+transdate.substring(4, 6)+"`"
//                    + "(`transdate`,`service_code`, `host_id`, `max_tps`,`avg_tps`) "
//                    + "VALUES ('"+transdate+"','"+service_code+"','"+hostname+"','"+max_tps+"','"+avg_tps+"')");
            st.executeUpdate("INSERT INTO `TMENU_TPS_"+transdate.substring(4, 6)+"`"
                    + "(`trans_date`,`service_code`, `host_id`, `max_tps`,`avg_tps`) "
                    + "VALUES ('"+transdate+"','"+service_code+"','"+hostname+"','"+max_tps+"','"+avg_tps+"')");
            
            
            return true;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return false;
    }
}
